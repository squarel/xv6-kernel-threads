#include "spinlock.h"
typedef int pthread_t;

pthread_t thread_create(void (*)(void*), void *);
int thread_join();

//typedef struct {
  //uint locked;       // Is the lock held?
//} lock_t;

typedef struct spinlock lock_t;

typedef struct {
    uint cond;
} cond_t;

void thread_lock_init(lock_t *);
void thread_lock_acquire(lock_t *);
void thread_lock_release(lock_t *);
void thread_exit();
int thread_detach(pthread_t tid);
int thread_cond_init(cond_t* cv);
int thread_cond_wait(cond_t* cv, lock_t* lk);
int thread_cond_signal(cond_t* cv);
