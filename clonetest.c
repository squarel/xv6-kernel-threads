#include "types.h"
#include "user.h"

#include "pthread.h"

pthread_t tid[30];
void test(void *arg);

int share_value;
lock_t lk;

int
main(int argc, char *argv[])
{
    int i = 0;
    int test_arg[2] = {0, 1};
    thread_lock_init(&lk);
    share_value = 0;
    /*pthread_t r;*/
    for(i = 0; i < 30; i++) {
        tid[i] = thread_create(&test, test_arg);
    }
    printf(1, "create threads done!\n");

    thread_detach(tid[10]);
    printf(1, "detach the 10th thread!\n");

    for(i = 0; i < 29; i++) {
        thread_join();
    }
    exit();
}

void
test(void *arg)
{
    printf(1, "\nargs: %d, %d\n", *(int *)arg, *((int *)arg + 1));
    int id = getpid();
    thread_lock_acquire(&lk);
    share_value++;
    thread_lock_release(&lk);
    printf(1, "I'm id:%d, share_value:%d\n", id, share_value);
    thread_exit();
}
