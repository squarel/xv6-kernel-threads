#include "types.h"
#include "user.h"
#include "param.h"
#include "x86.h"

#include "pthread.h"

// pthread library based on clone() and join()
#define PGSIZE 4096
#define MAX_THREAD 4


pthread_t
thread_create(void (*start_routine)(void*), void *arg)
{
    void *stack;
    pthread_t tid;

    stack = malloc(PGSIZE);
    /*if ((uint)stack % 0xa000 == 0 || (uint)stack % 0x2000 == 0) {*/
        /*stack = malloc(PGSIZE);*/
    /*}*/
    tid = clone(start_routine, arg, stack);
    printf(1, "new thread %d, stack: %p, fcn: %p\n", tid, stack, start_routine);
    return tid;
}

int
thread_join()
{
    void *stack;
    pthread_t tid;
    tid = join(&stack);
    printf(1, "free tid:%d, stack %p\n", tid, stack);
    free(stack);
    return tid;
}

int
thread_detach(pthread_t tid)
{
    printf(1, "detach tid:%d\n", tid);
    return detach(tid);
}

void thread_exit()
{
    exit();
}

void
thread_lock_init(lock_t *lk)
{
    lk->locked = 0;
}

void
thread_lock_acquire(lock_t *lk)
{
  while(xchg(&lk->locked, 1) != 0)
    ;
}

void
thread_lock_release(lock_t *lk)
{
  xchg(&lk->locked, 0);
}

int
thread_cond_init(cond_t *cv)
{
    if(cv) {
        cv->cond = 0;
        return 0;
    }
    return -1;
}

int
thread_cond_wait(cond_t *cv, lock_t *lk)
{
    return thread_sleep((void*)cv, lk);
}

int
thread_cond_signal(cond_t *cv)
{
    return thread_wakeup((void*)cv);
}
