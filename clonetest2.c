#include "types.h"
#include "user.h"

#include "pthread.h"

pthread_t tid[30];
void increment(void *arg);
void decrement(void *arg);

int share_value;
lock_t lk;
lock_t barrier_lk;
cond_t cv[15];
static int n = 0;

int
main(int argc, char *argv[])
{
    int i = 0;
    int test_arg[2] = {0, 1};
    thread_lock_init(&lk);
    thread_lock_init(&barrier_lk);
    for(i = 0; i < 15; i++)
        thread_cond_init(&cv[i]);
    share_value = 0;
    /*pthread_t r;*/
    for(i = 0; i < 15; i++) {
        tid[i] = thread_create(&decrement, test_arg);
    }
    printf(1, "create decrement threads done!\n");
    while( i < 30) {
        tid[i] = thread_create(&increment, test_arg);
        i++;
    }
    sleep(100);
    printf(1, "create increment threads done!\n");
    i = 0;
    thread_detach(tid[10]);
    printf(1, "detach the 10th thread!\n");

    while( i < 15) {
        thread_cond_signal((void *)&cv[i]);
        printf(1, "wake %d up!\n", i);
        i++;
    }
    i = 0;
    while(i < 29) {
        thread_join();
        i++;
    }
    exit();
}

void
increment(void *arg)
{
    printf(1, "\nargs: %d, %d\n", *(int *)arg, *((int *)arg + 1));
    int id = getpid();
    thread_lock_acquire(&lk);
    share_value++;
    thread_lock_release(&lk);
    printf(1, "I'm id:%d, increment share_value:%d\n", id, share_value);
    thread_exit();
}

void
decrement(void *arg)
{
    printf(1, "try\n");
    thread_cond_wait(&cv[n], &barrier_lk);
    n++;
    printf(1, "\nargs: %d, %d\n", *(int *)arg, *((int *)arg + 1));
    int id = getpid();
    thread_lock_acquire(&lk);
    share_value--;
    thread_lock_release(&lk);
    printf(1, "I'm id:%d, decrement share_value:%d\n", id, share_value);
    thread_exit();
}
